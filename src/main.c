//
// Created by aboba on 22.01.23.
//
#define _DEFAULT_SOURCE

#include <stdio.h>


#include "mem.h"
#include "mem_internals.h"

#include <stdbool.h>

#define SIZE 1190
#define HEAP_SIZE 11000

void remove_heap(void* heap){
    munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
}
typedef bool (*results) (void);
struct block_header* get_header(void* block){
    return (struct block_header*) (block - offsetof(struct block_header, contents));
}
bool test_1(){
    printf("Test 1:\n");
    void* heap = heap_init(HEAP_SIZE);
    if (heap == NULL){
        return false;
    }
    void* test_malloc = _malloc(10);
    debug_heap(stdout, heap);
    if (test_malloc == NULL){
        return false;
    }
    _free(test_malloc);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_SIZE}).bytes);
    return true;
}

bool test_2(){
    printf("Test 2s:\n");
    void* test_heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, test_heap);
    void* test_malloc_1 = _malloc(SIZE);
    void* test_malloc_2 = _malloc(SIZE);
    void* test_malloc_3 = _malloc(SIZE);
    debug_heap(stdout, test_heap);
    struct block_header* test_block_header_2 = get_header(test_malloc_2);
    if (test_heap == NULL || test_malloc_1 == NULL || test_malloc_2 == NULL || test_malloc_3 == NULL) return false;
    debug_heap(stdout, test_heap);
    _free(test_malloc_2);
    if (!test_block_header_2->is_free) return false;
    debug_heap(stdout, test_heap);
    _free(test_malloc_1);
    _free(test_malloc_3);
    remove_heap(test_heap);
    return true;
}

bool test_3(){
    printf("Test 3:\n");
    void* test_heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, test_heap);
    void* test_malloc_1 = _malloc(SIZE);
    void* test_malloc_2 = _malloc(SIZE);
    void* test_malloc_3 = _malloc(SIZE);
    void* test_malloc_4 = _malloc(SIZE);
    debug_heap(stdout, test_heap);
    struct block_header* test_block_header_2 = get_header(test_malloc_2);
    struct block_header* test_block_header_3 = get_header(test_malloc_3);
    if (test_heap == NULL || test_malloc_1 == NULL || test_malloc_2 == NULL || test_malloc_3 == NULL || test_malloc_4 == NULL) return false;
    _free(test_malloc_2);
    _free(test_malloc_3);
    if (!test_block_header_2->is_free || !test_block_header_3->is_free) return false;
    debug_heap(stdout, test_heap);
    _free(test_malloc_1);
    _free(test_malloc_4);
    remove_heap(test_heap);
    return true;
}

bool test_4(){
    printf("Test 4:\n");
    void* test_heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, test_heap);

    void* test_malloc_1 = _malloc(HEAP_SIZE);
    void* test_malloc_2 = _malloc(SIZE);
    debug_heap(stdout, test_heap);

    struct block_header* test_block_header_1 = get_header(test_malloc_1);
    struct block_header* test_block_header_2= get_header(test_malloc_2);

    if (test_block_header_1 == NULL || test_block_header_1->next != test_block_header_2 || test_block_header_1->capacity.bytes != HEAP_SIZE) return false;
    if (test_block_header_2 == NULL || test_block_header_2->capacity.bytes != SIZE) return false;
    debug_heap(stdout, test_heap);

    _free(test_malloc_1);
    _free(test_malloc_2);
    remove_heap(test_heap);
    return true;
}
bool test_5(){
    printf("Test 5:\n");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    void *test_mem = mmap(heap + HEAP_SIZE, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    if (!test_mem){
        return false;
    }
    void *block = _malloc(HEAP_SIZE);
    if (!heap || !block) {
        return false;
    }
    debug_heap(stdout, heap);
    _free(block);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_SIZE}).bytes);
    return true;
}

int main(){
    results tests[] = {test_1, test_2, test_3, test_4, test_5};
    uint32_t passed_tests = 0;
    printf("Run tests");
    for (uint32_t i = 0; i < 5; i++){
        if (tests[i]()){
            printf("Test passed: %d\n", i);
        }
        else
        {
            printf("Test faail: %d\n", i);
        }
        passed_tests++;
    }
    printf("Tests passed = %d\n", passed_tests);
}









